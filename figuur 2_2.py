# -*- coding: utf-8 -*-
"""
Created on Tue Oct  8 13:04:59 2019

@author: buijs_sn
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

sns.set("paper")
from paths import set_paths


paths = set_paths()

#############   Figure 2.2   Clustered stacked column met stoffenlijst vs. gemeten stoffen
def clustered_stacked_column(
    n2014, n2017, n2018, n2019, n2020, m2014, m2017, m2018, m2019, m2020
):
    legend = [
        "Akkerbouw",
        "Bloembollen",
        "Boomkwekerij",
        "Fruitteelt",
        "Glastuinbouw",
        "Mais en grasland",
        "Wintertarwe",
    ]
    plt.figure(figsize=[12, 6])
    with sns.axes_style("white"):
        sns.set_style("ticks")
        sns.set_context("talk")

        # plot details
        bar_width = 0.35
        epsilon = 0.015
        pos_bar_positions = np.arange(len(n2014))
        neg_bar_positions = pos_bar_positions + bar_width

        # make bar plots
        plt.bar(
            pos_bar_positions,
            n2014,
            bar_width - epsilon,
            color="#08519c",
            label="Stoffenlijst 2014        ",
            edgecolor="black",
            linewidth=0.5,
        )
        plt.bar(
            pos_bar_positions,
            n2017,
            bar_width - epsilon,
            bottom=n2014,
            color="#3182bd",
            label="Stoffenlijst 2017        ",
            edgecolor="black",
            linewidth=0.5,
        )
        plt.bar(
            pos_bar_positions,
            n2018,
            bar_width - epsilon,
            bottom=n2014 + n2017,
            color="#6baed6",
            label="Stoffenlijst 2018        ",
            edgecolor="black",
            linewidth=0.5,
        )
        plt.bar(
            pos_bar_positions,
            n2019,
            bar_width - epsilon,
            bottom=n2014 + n2017 + n2018,
            color="#bdd7e7",
            label="Stoffenlijst 2019        ",
            edgecolor="black",
            linewidth=0.5,
        )
        plt.bar(
            pos_bar_positions,
            n2020,
            bar_width - epsilon,
            bottom=n2014 + n2017 + n2018 + n2019,
            color="#eff3ff",
            label="Stoffenlijst 2020        ",
            edgecolor="black",
            linewidth=0.5,
        )
        plt.bar(
            neg_bar_positions,
            m2014,
            bar_width - epsilon,
            color="#006d2c",
            label="Gemeten (stoffenlijst 2014)",
            edgecolor="black",
            linewidth=0.5,
        )
        plt.bar(
            neg_bar_positions,
            m2017,
            bar_width - epsilon,
            bottom=m2014,
            color="#31a354",
            label="Gemeten (stoffenlijst 2017)",
            edgecolor="black",
            linewidth=0.5,
        )
        plt.bar(
            neg_bar_positions,
            m2018,
            bar_width - epsilon,
            bottom=m2014 + m2017,
            color="#74c476",
            label="Gemeten (stoffenlijst 2018)",
            edgecolor="black",
            linewidth=0.5,
        )
        plt.bar(
            neg_bar_positions,
            m2019,
            bar_width - epsilon,
            bottom=m2014 + m2017 + m2018,
            color="#bae4b3",
            label="Gemeten (stoffenlijst 2019)",
            edgecolor="black",
            linewidth=0.5,
        )
        plt.bar(
            neg_bar_positions,
            m2020,
            bar_width - epsilon,
            bottom=m2014 + m2017 + m2018 + m2019,
            color="#edf8e9",
            label="Gemeten (stoffenlijst 2020)",
            edgecolor="black",
            linewidth=0.5,
        )
        plt.xticks(neg_bar_positions, legend, rotation=45, ha="right")
        # plt.setp( ax.xaxis.get_majorticklabels(), rotation=-45, ha="left" )
        plt.ylabel("Aantal stoffen")
        plt.legend(loc="best", bbox_to_anchor=(1.05, 1))
        sns.despine()
        plt.tight_layout()


# Laad data
stoffenlijst = pd.read_excel(paths["stoffenlijst"])
stoffenlijst.Teelt = stoffenlijst.Teelt.str.lower()
metingen_jaar = pd.read_excel(paths["metingen_jaar"])


stof2014 = (
    stoffenlijst[stoffenlijst["Datum_opname LM-GBM"].dt.year == 2014]
    .groupby("Teelt")["Stof"]
    .unique()
)
stof2017 = (
    stoffenlijst[stoffenlijst["Datum_opname LM-GBM"].dt.year == 2017]
    .groupby("Teelt")["Stof"]
    .unique()
)
stof2018 = (
    stoffenlijst[stoffenlijst["Datum_opname LM-GBM"].dt.year == 2018]
    .groupby("Teelt")["Stof"]
    .unique()
)
stof2019 = (
    stoffenlijst[stoffenlijst["Datum_opname LM-GBM"].dt.year == 2019]
    .groupby("Teelt")["Stof"]
    .unique()
)
stof2020 = (
    stoffenlijst[stoffenlijst["Datum_opname LM-GBM"].dt.year == 2020]
    .groupby("Teelt")["Stof"]
    .unique()
)
mtotaal = metingen_jaar.groupby("MLCMN_TEELT")["STOF_NAAM_SAM"].unique()

m2014 = {}
for index, row in mtotaal.iteritems():
    m2014[index] = len([x for x in row if x in stof2014[index]]), len(stof2014[index])
m2017 = {}
for index, row in mtotaal.iteritems():
    try:
        m2017[index] = (
            len([x for x in row if x in stof2017[index]]),
            len(stof2017[index]),
        )
    except:
        m2017[index] = 0, 0
m2018 = {}
for index, row in mtotaal.iteritems():
    try:
        m2018[index] = (
            len([x for x in row if x in stof2018[index]]),
            len(stof2018[index]),
        )
    except:
        m2018[index] = 0, 0
m2019 = {}
for index, row in mtotaal.iteritems():
    try:
        m2019[index] = (
            len([x for x in row if x in stof2019[index]]),
            len(stof2019[index]),
        )
    except:
        m2019[index] = 0, 0
m2020 = {}
for index, row in mtotaal.iteritems():
    try:
        m2020[index] = (
            len([x for x in row if x in stof2020[index]]),
            len(stof2020[index]),
        )
    except:
        m2020[index] = 0, 0
m2014 = pd.DataFrame.from_dict(
    m2014, orient="index", columns=["Gemeten", "Stoffenlijst"]
)
m2017 = pd.DataFrame.from_dict(
    m2017, orient="index", columns=["Gemeten", "Stoffenlijst"]
)
m2018 = pd.DataFrame.from_dict(
    m2018, orient="index", columns=["Gemeten", "Stoffenlijst"]
)
m2019 = pd.DataFrame.from_dict(
    m2019, orient="index", columns=["Gemeten", "Stoffenlijst"]
)
m2020 = pd.DataFrame.from_dict(
    m2020, orient="index", columns=["Gemeten", "Stoffenlijst"]
)
clustered_stacked_column(
    m2014["Stoffenlijst"].values,
    m2017["Stoffenlijst"].values,
    m2018["Stoffenlijst"].values,
    m2019["Stoffenlijst"].values,
    m2020["Stoffenlijst"].values,
    m2014["Gemeten"].values,
    m2017["Gemeten"].values,
    m2018["Gemeten"].values,
    m2019["Gemeten"].values,
    m2020["Gemeten"].values,
)

plt.savefig(paths["output"] / "Gemeten stoffen per teelt", dpi=600)
###################################################################################
