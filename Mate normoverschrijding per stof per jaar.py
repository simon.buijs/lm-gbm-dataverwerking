# -*- coding: utf-8 -*-
"""
Created on Mon Sep  9 11:30:59 2019
Normoverschrijdingen per stof per teelt per jaar
@author: buijs_sn
"""
import pandas as pd
from functions import selectie
from paths import set_paths
import seaborn as sns

sns.set_context("talk")
sns.set_style("white")
import matplotlib.pyplot as plt
from constants import YEAR

## Filenames
paths = set_paths()
normovers = pd.read_excel(paths["tabelH"])
topstoffen = pd.read_excel(paths["tabelD"])


for norm, stof, teelt in (
    normovers[["NORM_OMSCHRIJVING", "STOF_NAAM_SAM", "MLCMN_TEELT"]]
    .drop_duplicates()
    .itertuples(index=False)
):
    df = selectie(normovers, teelt=teelt, stof=stof, norm=norm)
    df = df.pivot(index="JAAR", columns="KLASSE_OMSCHRIJVING", values="NMPT")
    columns = [
        "geen metingen",
        "niet toetsbaar",
        "niet aangetroffen",
        "aangetroffen",
        "> norm",
        "> 5*norm",
    ]
    try:
        df["> norm"]
    except:
        try:
            df["> 5*norm"]
        except:
            try:
                df["niet toetsbaar"]
            except:
                continue
    for column in columns:
        try:
            df[column]
        except:
            df[column] = 0
    df = df[columns]
    ax = df.plot(
        kind="bar",
        stacked=True,
        color=["lightgrey", "grey", "deepskyblue", "limegreen", "yellow", "red"],
        figsize=(9, 6),
    )
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(
        reversed(handles), reversed(labels), loc="center left", bbox_to_anchor=(1, 0.5)
    )
    ax.set_ylabel("Aantal meetlocaties")
    ax.set_xlabel("")
    sns.despine()
    plt.tight_layout()
    plt.xticks(rotation=45)
    norm = norm.replace("/", "_")
    path = paths["output"] / f"Normoverschrijding stof/{stof}"
    path.mkdir(exist_ok=True, parents=True)
    plt.savefig(path / f"{stof} {teelt} {norm} normoverschrijding per jaar")
    plt.close()
####################################################################################
### Voor alle teelten
for norm, stof in (
    normovers[["NORM_OMSCHRIJVING", "STOF_NAAM_SAM"]]
    .drop_duplicates()
    .itertuples(index=False)
):
    teelt = "Totaal"
    df = selectie(normovers, stof=stof, norm=norm)
    if df["MLCMN_TEELT"].nunique() == 1:
        continue
    df = (
        df.groupby(["JAAR", "STOF_NAAM_SAM", "KLASSE_OMSCHRIJVING"])["NMPT"]
        .sum()
        .reset_index()
    )
    df = df.pivot(index="JAAR", columns="KLASSE_OMSCHRIJVING", values="NMPT")
    columns = [
        "geen metingen",
        "niet toetsbaar",
        "niet aangetroffen",
        "aangetroffen",
        "> norm",
        "> 5*norm",
    ]
    try:
        df["> norm"]
    except:
        try:
            df["> 5*norm"]
        except:
            try:
                df["niet toetsbaar"]
            except:
                continue
    for column in columns:
        try:
            df[column]
        except:
            df[column] = 0
    df = df[columns]
    ax = df.plot(
        kind="bar",
        stacked=True,
        color=["lightgrey", "grey", "deepskyblue", "limegreen", "yellow", "red"],
        figsize=(9, 6),
    )

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(
        reversed(handles), reversed(labels), loc="center left", bbox_to_anchor=(1, 0.5)
    )
    ax.set_ylabel("Aantal meetlocaties")
    ax.set_xlabel("")
    sns.despine()
    plt.tight_layout()
    plt.xticks(rotation=45)
    norm = norm.replace("/", "_")
    path = paths["output"] / f"Normoverschrijding stof/{stof}"
    path.mkdir(exist_ok=True, parents=True)
    plt.savefig(path / f"{stof} {teelt} {norm} normoverschrijding per jaar")
    plt.close()
######################## Figuur 3.4 - Top 5 stoffen somindex per jaar #########
sns.set_context("notebook")
for teelt in topstoffen["MLCMN_TEELT"].unique():
    for norm in ["JG-MKN/MTR", "MAC-MKN"]:
        # Bepaal top5
        top5 = selectie(topstoffen, norm=norm, jaar=YEAR, teelt=teelt)
        top5 = top5.sort_values("RANK")["STOF_NAAM_SAM"][:5]
        # Selecteer top 5
        df = selectie(topstoffen, norm=norm, teelt=teelt)
        df = df[df["STOF_NAAM_SAM"].isin(top5)]
        df = df.pivot(index="STOF_NAAM_SAM", columns="JAAR", values="INDEX")
        df = df.reindex(top5)

        ax = df.plot(kind="bar", figsize=(9, 6))
        ax.set_ylabel("Index [-]")
        ax.legend(loc="center left", bbox_to_anchor=(1, 0.5))
        ax.set_xlabel("")
        sns.despine()
        plt.xticks(rotation=45, ha="right")
        plt.tight_layout()
        norm = norm.replace("/", "_")
        p = paths["output"] / "Top5 stoffen"
        p.mkdir(exist_ok=True, parents=True)
        plt.savefig(p / f"Figuur 3_4 Top 5 stoffen {norm} {teelt}")
        plt.close()
    ax.set_ylabel("Aantal meetlocaties")
    ax.set_xlabel("")
    sns.despine()
    plt.tight_layout()
    plt.xticks(rotation=45)
    norm = norm.replace("/", "_")
    path = paths["output"] / f"Normoverschrijding stof/{stof}"
    path.mkdir(exist_ok=True, parents=True)
    plt.savefig(path / f"{stof} {teelt} {norm} normoverschrijding per jaar")
    plt.close()
######################## Figuur 3.4 - Top 5 stoffen somindex per jaar #########
sns.set_context("notebook")
for teelt in topstoffen["MLCMN_TEELT"].unique():
    for norm in ["JG-MKN/MTR", "MAC-MKN"]:
        # Bepaal top5
        top5 = selectie(topstoffen, norm=norm, jaar=YEAR, teelt=teelt)
        top5 = top5.sort_values("RANK")["STOF_NAAM_SAM"][:5]
        # Selecteer top 5
        df = selectie(topstoffen, norm=norm, teelt=teelt)
        df = df[df["STOF_NAAM_SAM"].isin(top5)]
        df = df.pivot(index="STOF_NAAM_SAM", columns="JAAR", values="INDEX")
        df = df.reindex(top5)

        ax = df.plot(kind="bar", figsize=(9, 6))
        ax.set_ylabel("Index [-]")
        ax.legend(loc="center left", bbox_to_anchor=(1, 0.5))
        ax.set_xlabel("")
        sns.despine()
        plt.xticks(rotation=45, ha="right")
        plt.tight_layout()
        norm = norm.replace("/", "_")
        p = paths["output"] / "Top5 stoffen"
        p.mkdir(exist_ok=True, parents=True)
        plt.savefig(p / f"Figuur 3_4 Top 5 stoffen {norm} {teelt}")
        plt.close()
