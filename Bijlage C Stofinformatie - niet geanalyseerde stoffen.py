# -*- coding: utf-8 -*-
"""
Created on Tue Sep 10 14:07:04 2019

@author: buijs_sn
"""
import pandas as pd
from functions import selectie
from paths import set_paths
from constants import YEAR

## Filenames
paths = set_paths()

# load data
normov = pd.read_excel(paths['tabelH'])
metingen = pd.read_excel(paths['metingen_jaar'])
metingen = selectie(metingen, jaar=YEAR)
metingen['MLCMN_TEELT'] = metingen['MLCMN_TEELT'].str.capitalize()
stoffenlijst = pd.read_excel(paths['stoffenlijst'])
stoffenlijst['Teelt'] = stoffenlijst['Teelt'].str.capitalize()

# Krijg de unieke stoffen per teelt (zowel in stoffenlijst als in metingen)
stoffen = stoffenlijst[['Teelt','Stof','CAS-nummer','Aquonaam']].drop_duplicates()
meetstoffen = metingen[['MLCMN_TEELT','STOF_NAAM_SAM']].drop_duplicates()

# Kijk naar de verschillen
diff_df = pd.merge(stoffen, meetstoffen,left_on=['Teelt','Stof'], right_on=['MLCMN_TEELT','STOF_NAAM_SAM'], how='outer', indicator='Exist')
diff_df = diff_df.loc[diff_df['Exist'] != 'both']
diff_df['Exist'] = diff_df['Exist'].cat.rename_categories({'left_only':'Stoffenlijst','right_only':'Gemeten'})
diff_df = diff_df.sort_values(by='Teelt')
diff_df.to_excel(paths['output'] / 'Bijlage C1 Verschil tussen stoffenlijst en gemeten stoffen.xlsx', index=False)