# -*- coding: utf-8 -*-
"""
Created on Wed Oct  7 10:18:10 2020
Percentage niet toetsbaar over de jaren heen.
@author: buijs_sn
"""
import matplotlib.pyplot as plt
import pandas as pd
from functions import selectie
from paths import set_paths
import seaborn as sns; sns.set_context('talk')

## Filenames
paths = set_paths()

nt = pd.read_excel(paths["tabelA"])
nt = selectie(nt, norm='JG-MKN/MTR', teelt='Alle_teelten')
nt = nt[nt['STOF_NAAM_SAM'] == 'Alle_meetnet_stoffen']
nt = nt[nt.index >2013]
nt = nt.set_index('JAAR')

nt[nt.index>2013]['P_NT'].plot(kind='bar',legend = None, figsize=(9,6))
sns.despine()
plt.xticks(rotation=45)
plt.xlabel('')
plt.tight_layout()
p = paths['output']
plt.savefig(p / 'Percentage NT')

nt['N_NT'].plot(kind='bar',legend = None, figsize=(9,6))
sns.despine()
plt.xticks(rotation=45)
plt.xlabel('')
plt.tight_layout()
p = paths['output']
plt.savefig(p / 'Aantal NT')



df = pd.read_excel(paths["tabelB"])
df = df[df['NORMKLAS']==11]

nt = df[df['BOVEN_ONDER_NORM_BESCHRIJVING']=='Niet toetsbaar']
nt = nt.groupby(['JAAR','code']).count()['BOVEN_ONDER_NORM_BESCHRIJVING']
nt.to_csv('test.csv', sep=';')
aantal = nt.groupby('JAAR').mean()

tot = df.groupby(['JAAR','code']).count()['BOVEN_ONDER_NORM_BESCHRIJVING']
tot.plot(kind='bar',legend = None, figsize=(9,6))
sns.despine()
plt.xticks(rotation=45)
plt.xlabel('')
plt.tight_layout()
p = paths['output']
plt.savefig(p / 'Totaal aantal stoffen')

percent = (nt/tot).groupby('JAAR').mean()

(percent*100).plot(kind='bar',legend = None, figsize=(9,6))
sns.despine()
plt.xticks(rotation=45)
plt.xlabel('')
plt.tight_layout()

nt = pd.read_excel(paths["tabelA"])
nt = selectie(nt, norm='JG-MKN/MTR', teelt='Alle_teelten')
nt = nt[nt['STOF_NAAM_SAM'] == 'Alle_meetnet_stoffen']
nt = nt[nt.index >2013]
nt = nt.set_index('JAAR')
nt.rename({'N_NT':'Aantal niet-toetsbaar (rechts)','N_TOT':'Totaal aantal stoffen'}, axis=1, inplace=True)
nt[['Totaal aantal stoffen','Aantal niet-toetsbaar (rechts)']].plot(kind='bar',figsize=(9,6), secondary_y=['Aantal niet-toetsbaar (rechts)'], legend=True, mark_right=False)
plt.xticks(rotation=45)
plt.tight_layout()
p = paths['output']
plt.savefig(p / 'Aantal NT en totaal')
