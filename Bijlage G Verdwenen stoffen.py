# -*- coding: utf-8 -*-
"""
Created on Tue Sep 10 15:40:55 2019
Bijlage G Stoffen verdwenen uit index voor JG-MKN en MAC-MKN
@author: buijs_sn
"""
import pandas as pd
from functions import selectie
from paths import set_paths
from constants import YEAR, LAST_YEAR, START_YEAR

## Filenames
paths = set_paths()
normover = pd.read_excel(paths['tabelD'])
stoffenlijst = pd.read_excel(paths['stoffenlijst'])

jaar_vervallen = stoffenlijst[['Teelt','Stof','Vervallen toelating']].copy()

for norm in normover['NORM_OMSCHRIJVING'].unique():
    df = selectie(normover, norm=norm)
    df = df.pivot_table(values='INDEX',index=['MLCMN_TEELT','STOF_NAAM_SAM'],columns='JAAR',aggfunc=sum).reset_index()
    sel1 = df[YEAR] == 0
    sel2 = df[LAST_YEAR] != 0 
    sel3 = df[LAST_YEAR].notna()
    df = df[sel1 & sel2 & sel3]
    years = list(range(LAST_YEAR,START_YEAR-1,-1))
    df = df[['MLCMN_TEELT','STOF_NAAM_SAM',*years]]
    df = pd.merge(df, jaar_vervallen, left_on=['MLCMN_TEELT','STOF_NAAM_SAM'], right_on=['Teelt','Stof'], how='left')
    df = df.drop(['Teelt','Stof'], axis=1)
    
    norm = norm.replace('/','_')
    df.to_excel(paths['output'] / f'Bijlage G Verdwenen stoffen {norm}.xlsx', index=False)
