# -*- coding: utf-8 -*-
"""
Created on Wed Sep 11 13:38:03 2019
Hoofdstuk 3.4 Concentraties van niet normoverschrijdende stoffen
@author: buijs_sn
"""
import pandas as pd
import os
from functions import selectie
from paths import set_paths
from constants import YEAR

## Filenames
paths = set_paths()

## Laad data
normover = pd.read_excel(paths["tabelH"])
conc = pd.read_excel(paths["tabelL"])
normen = pd.read_excel(paths["normen"])
normen["STOF_NAAM_SAM"] = normen["STOF_NAAM_SAM"].astype("str")
metingen = pd.read_excel(paths["metingen"])

# Bepaal niet norm-overschrijdende stoffen
df = selectie(normover, jaar=YEAR, norm="JG-MKN/MTR")
df = df.pivot_table(
    index=["MLCMN_TEELT", "STOF_NAAM_SAM"], columns="KLASSE_OMSCHRIJVING", values="NMPT"
)
df = df.fillna(0).reset_index()
df["normoverschrijding"] = df["> 5*norm"] + df["> norm"]
ds = df[df["normoverschrijding"] == 0]

## Bepaal gemiddelde waarden per jaar
df_conc = pd.merge(ds, conc, on=["MLCMN_TEELT", "STOF_NAAM_SAM"], how="inner")
df_conc = df_conc.drop(["STOFNR", "MLCMN_NR"], axis=1)

df_conc = df_conc[df_conc["CONC_RAPGRENS"] == 1]
df_conc = df_conc.pivot_table(
    index=["MLCMN_TEELT", "STOF_NAAM_SAM"], columns="JAAR", values="CONCAVG"
)
df = df_conc.pct_change(axis="columns") * 100
df.columns = ["%" + str(x) for x in df.columns]
df = df_conc.join(df.drop("%2014", axis=1)).reset_index()
df["STOF_NAAM_SAM"] = df["STOF_NAAM_SAM"].astype("str")
## Koppel normen aan de dataset
df = pd.merge(
    df,
    normen.drop(["STOF_NR_SAM", "CAS_NR", "DWN", "Normen in ug/l"], axis=1),
    on=["STOF_NAAM_SAM"],
    how="left",
)
df = pd.merge(df, ds, on=["MLCMN_TEELT", "STOF_NAAM_SAM"], how="left")

df.to_excel(paths["output"] / "H3_4 conc niet normoverschrijdend.xlsx", index=False)


### Geen normen (normenlijst)
## Maak een overzicht bij welke gemeten stoffen geen norm hoort
df = selectie(metingen, jaar=YEAR)
df = df[["MLCMN_TEELT", "STOF_NAAM_SAM"]].drop_duplicates()
df = pd.merge(df, normen, on="STOF_NAAM_SAM", how="left")

sel1 = df["MTR"].isna()
sel2 = df["JG-MKN"].isna()
sel3 = df["MAC-MKN"].isna()
geen_norm = df[sel1 & sel2 & sel3]

## Kijk bij de resultaten van de metingen of deze stoffen uberhaubt zijn aangetroffen
df = selectie(metingen, jaar=YEAR)
df = df.groupby(['MLCMN_TEELT','STOF_NAAM_SAM']).agg({'MEETWAARDE2':'mean','BDETGRENS':'sum','MEETNET_VOLG':'count'}).reset_index()
df = pd.merge(geen_norm, df, on=["MLCMN_TEELT", "STOF_NAAM_SAM"], how="left")
df = df.drop(["MTR", "DWN", "JG-MKN", "MAC-MKN", "Normen in ug/l"], axis=1)
df = df.rename({'BDETGRENS':'AANGETROFFEN METINGEN','MEETNET_VOLG':'AANTAL METINGEN'})
df.to_excel(paths["output"] / "Bijlage H H3_4_2 geen normen.xlsx", index=False)