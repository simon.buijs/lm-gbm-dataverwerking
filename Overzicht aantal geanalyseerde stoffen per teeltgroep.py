# -*- coding: utf-8 -*-
"""
Created on Mon Sep  9 15:49:26 2019
Tabel 3.2  Overzicht aantal geanalyseerde stoffen per teeltgroep
@author: buijs_sn
"""
import pandas as pd
from functions import selectie
from paths import set_paths

## Filenames
paths = set_paths()

## Inlezen data
stoffenlijst = pd.read_excel(paths["stoffenlijst"])
stoffenlijst["Teelt"] = stoffenlijst["Teelt"].str.capitalize()
metingen = pd.read_excel(paths["metingen"])
metingen["MLCMN_TEELT"] = metingen["MLCMN_TEELT"].str.capitalize()
topstoffen = pd.read_excel(paths["tabelD"])
topstoffen["MLCMN_TEELT"] = topstoffen["MLCMN_TEELT"].str.capitalize()
normovers = pd.read_excel(paths["tabelH"])
normovers["MLCMN_TEELT"] = normovers["MLCMN_TEELT"].str.capitalize()


###########   Analyse tabel 3.2 Overzicht per teeltgroep van aantal (#) te analyseren stoffen  ###############
for norm in ["JG-MKN/MTR", "MAC-MKN"]:

    ## Aantal stoffen in stoffenlijst per teelt
    ## N stoffenlijst
    nstoffen_per_teelt2020 = stoffenlijst.groupby("Teelt")["Stof"].nunique()

    # N gemeten
    df = (
        metingen.groupby(["MLCMN_TEELT", "JAAR"])["STOF_NAAM_SAM"]
        .nunique()
        .reset_index()
    )
    df.rename({"STOF_NAAM_SAM": "Stoffen geanalyseerd"}, axis=1, inplace=True)
    df["Stoffen te analyseren"] = df["MLCMN_TEELT"].map(nstoffen_per_teelt2020)
    df = df[df.columns[[0, 1, 3, 2]]]

    # N geanalyseerd per norm
    nanalyse = selectie(topstoffen, norm=norm)
    nanalyse = (
        nanalyse.groupby(["MLCMN_TEELT", "JAAR"])["STOF_NAAM_SAM"]
        .nunique()
        .reset_index()
    )
    df = pd.merge(df, nanalyse, how="left", on=["MLCMN_TEELT", "JAAR"])
    df.rename(
        {"STOF_NAAM_SAM": "Stoffen met norm geanalyseerd"}, axis="columns", inplace=True
    )

    # N stoffen normoverschrijdend per teelt
    sel1 = normovers["NORM_OMSCHRIJVING"] == norm
    sel2 = normovers["KLASSE"].isin([2, 3])
    normjgmkn = normovers[sel1 & sel2]
    normjgmkn = (
        normjgmkn.groupby(["MLCMN_TEELT", "JAAR"])["STOF_NAAM_SAM"]
        .nunique()
        .reset_index()
    )
    df = pd.merge(df, normjgmkn, how="left", on=["MLCMN_TEELT", "JAAR"])
    df.rename(
        {"STOF_NAAM_SAM": "Stoffen normoverschrijdend"}, axis="columns", inplace=True
    )
    df["Totaal % stoffen normoverschrijdend"] = (
        df["Stoffen normoverschrijdend"] / df["Stoffen met norm geanalyseerd"] * 100
    ).round(0)
    if norm == "JG-MKN/MTR":
        df.to_excel(
            paths["output"]
            / "Overzicht aantal geanalyseerde stoffen per teeltgroep.xlsx",
            index=False,
        )
    else:
        df.to_excel(
            paths["output"]
            / "Overzicht aantal geanalyseerde stoffen per teeltgroep MAC.xlsx",
            index=False,
        )
    #################################################################################

    #### Tabel 3.3 Alle teelten samen
    nstof = stoffenlijst["Stof"].nunique()
    df = metingen.groupby(["JAAR"])["STOF_NAAM_SAM"].nunique().reset_index()
    df.rename({"STOF_NAAM_SAM": "Stoffen geanalyseerd"}, axis=1, inplace=True)
    df["Stoffen te analyseren"] = nstof
    df = df[df.columns[[0, 2, 1]]]
    jgmkn = topstoffen[topstoffen["NORM_OMSCHRIJVING"] == norm].copy()
    jgmkn = jgmkn.groupby("JAAR")["STOF_NAAM_SAM"].nunique().reset_index()
    df = pd.merge(df, jgmkn, how="left", on="JAAR")
    df.rename(
        {"STOF_NAAM_SAM": "Stoffen met norm geanalyseerd"}, axis="columns", inplace=True
    )
    normjgmkn = normovers[sel1 & sel2]
    normjgmkn = normjgmkn.groupby("JAAR")["STOF_NAAM_SAM"].nunique().reset_index()
    df = pd.merge(df, normjgmkn, how="left", on="JAAR")
    df.rename(
        {"STOF_NAAM_SAM": "Stoffen normoverschrijdend"}, axis="columns", inplace=True
    )
    df["Totaal % stoffen normoverschrijdend"] = (
        df["Stoffen normoverschrijdend"] / df["Stoffen met norm geanalyseerd"] * 100
    ).round(0)
    if norm == "JG-MKN/MTR":
        df.to_excel(
            paths["output"]
            / "Overzicht aantal geanalyseerde stoffen Alle teelten JGMKN.xlsx",
            index=False,
        )
    else:
        df.to_excel(
            paths["output"]
            / "Overzicht aantal geanalyseerde stoffen Alle teelten MAC.xlsx",
            index=False,
        )
