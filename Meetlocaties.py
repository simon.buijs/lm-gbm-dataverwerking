# -*- coding: utf-8 -*-
"""
Created on Tue Sep 10 13:46:48 2019
Meetlocaties LM-GBM Bijlage A
@author: buijs_sn
"""
import pandas as pd

from constants import WS_MAP
from paths import set_paths

paths = set_paths()
## Filenames
metingen = pd.read_excel(paths["metingen_jaar"])
metingen["WBHCODE_OMSCHRIJVING"] = metingen["WBHCODE_OMSCHRIJVING"].map(WS_MAP)
meetlocaties = metingen[
    ["WBHCODE_OMSCHRIJVING", "MLCMN_TEELT", "code", "XCOORD_M", "YCOORD_M"]
].drop_duplicates()
meetlocaties["MLCMN_TEELT"] = meetlocaties["MLCMN_TEELT"].str.capitalize()
meetlocaties = meetlocaties.sort_values(["WBHCODE_OMSCHRIJVING", "MLCMN_TEELT", "code"])
meetlocaties.to_excel(paths["output"] / "Bijlage A Meetlocaties.xlsx", index=False)
