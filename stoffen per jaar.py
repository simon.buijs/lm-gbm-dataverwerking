# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 12:09:53 2020

@author: buijs_sn
"""
import pandas as pd
import matplotlib.pyplot as plt 
import seaborn as sns; 
from matplotlib.colors import ListedColormap
from paths import set_paths

path = r'p:\landelijk-meetnet-gbm\LM-GBM 2020\meetnet\Stoffenlijst + stofinfo\Stoffenlijst september 2020_final.xlsx'
paths = set_paths()

sns.axes_style("white")
sns.set_style("ticks")
sns.set_context("talk")
df = pd.read_excel(path, parse_dates=True, sheet_name='totaal overzicht')
df = df[df['Datum_stoppen in LM-GBM'].isna()]
df['Jaar opname'] = df['Datum_opname LM-GBM'].dt.year
df['Teelt'] = df['Teelt'].str.capitalize()
stoffen = df.pivot_table(index='Teelt',columns = 'Jaar opname',values='Stof', aggfunc='count')
stoffen = stoffen.fillna(0)

# stoffen[2017] = stoffen[2014]+ stoffen[2017]
# stoffen[2018] = stoffen[2017]+ stoffen[2018]
# stoffen[2019] = stoffen[2018]+ stoffen[2019]
# stoffen[2020] = stoffen[2019]+ stoffen[2020]
# stoffen[2021] = stoffen[2020]+ stoffen[2021]
# stoffen.plot(kind='bar')


ax = stoffen.plot(kind='bar',legend=True, stacked=True, figsize=(9,6),colormap=ListedColormap(sns.color_palette("GnBu", 6)))
plt.ylabel('Aantal stoffen')
sns.despine()
plt.xticks(rotation=45)
plt.xlabel('')
plt.tight_layout()
plt.legend(bbox_to_anchor=(1.05, 1))
p = paths['output'] / 'Stoffen per jaar/'
p.mkdir(exist_ok=True, parents=True)

plt.savefig(p / 'Stoffen per jaar')
plt.close()
