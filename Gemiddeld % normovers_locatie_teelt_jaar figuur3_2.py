# -*- coding: utf-8 -*-
"""
Created on Mon Sep  9 14:29:56 2019
Figuur 3.2 Gemiddeld percentage normoverschrijdende stoffen per locatie per teeltgroep
@author: buijs_sn
"""
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from paths import set_paths

sns.set("talk")
## Filenames
paths = set_paths()
## Inlezen data
topteelt = pd.read_excel(paths["tabelE"])

topteelt = topteelt[["JAAR", "NORM_OMSCHRIJVING", "MLCMN_TEELT", "INDEX"]]
topteelt.columns = ["jaar", "norm", "teelt", "Gemiddeld % > norm"]

topteelt = topteelt.sort_values("norm")
sns.set(rc={"figure.figsize": (9, 9)})
sns.set_style("whitegrid")
sns.set_context("notebook", font_scale=1.0)
ax = sns.catplot(
    x="teelt",
    y="Gemiddeld % > norm",
    hue="jaar",
    row="norm",
    data=topteelt,
    kind="bar",
    order=[
        "bloembollen",
        "glastuinbouw",
        "boomkwekerij",
        "akkerbouw",
        "wintertarwe",
        "fruitteelt",
        "mais en grasland",
    ],
)
ax.set_xticklabels(rotation=65, horizontalalignment="right")
plt.tight_layout()

plt.savefig(paths["output"] / "fig3_2_oud", dpi=600)


# Op een andere manier berekend
normovers = pd.read_excel(paths["tabelM"])

normovers = normovers[
    [
        "JAAR",
        "NORM_OMSCHRIJVING",
        "MLCMN_TEELT",
        "NSTOF_NORMOVERSCHRIJDING",
        "NSTOF_GEMETEN",
    ]
]
normovers.columns = [
    "jaar",
    "norm",
    "teelt",
    "# stoffen normoverschrijdend",
    "# stoffen gemeten",
]

normovers = normovers[normovers["teelt"] != "Alle_teelten"]
normovers["Percentage normoverschrijdend"] = (
    normovers["# stoffen normoverschrijdend"] / normovers["# stoffen gemeten"] * 100
)
normovers = normovers.sort_values(by=["norm"])
sns.set(rc={"figure.figsize": (9, 9)})
sns.set_style("white")
sns.set_context("notebook", font_scale=1.0)
order = (
    normovers[normovers.jaar == 2020][normovers.norm == "JG-MKN/MTR"]
    .sort_values("Percentage normoverschrijdend", ascending=False)["teelt"]
    .values
)

ax = sns.catplot(
    x="teelt",
    y="Percentage normoverschrijdend",
    hue="jaar",
    row="norm",
    data=normovers,
    kind="bar",
    order=order,
    aspect=1.3,
)
ax.set_xticklabels(rotation=65, horizontalalignment="right")
plt.tight_layout()
plt.savefig(paths["output"] / "fig3_2_nieuw", dpi=600)
