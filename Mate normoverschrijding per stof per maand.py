# -*- coding: utf-8 -*-
"""
Created on Mon Sep  9 11:30:59 2019
Normoverschrijdingen per stof per teelt per maand (2019)
@author: buijs_sn
"""
import pandas as pd
import seaborn as sns; sns.set_context('talk')
sns.set_style('white')
import matplotlib.pyplot as plt
from functions import selectie
from paths import set_paths
from constants import YEAR
from matplotlib.ticker import MaxNLocator

## Filenames
paths = set_paths()
normovers = pd.read_excel(paths['tabelJ'])
normovers.rename({'STOFNAAM':'STOF_NAAM_SAM'}, axis='columns', inplace=True)

for norm, stof, teelt in normovers[['NORM_OMSCHRIJVING','STOF_NAAM_SAM','MLCMN_TEELT']].drop_duplicates().itertuples(index=False):
    df = selectie(normovers, teelt=teelt, stof=stof, norm=norm, jaar=YEAR)
    df = df.set_index('MAAND')[['NMPT_NORMOV_METING','NMPT_ONDER_NORM_METING','NMPT_NT_METING']]
    if df[['NMPT_NORMOV_METING', 'NMPT_NT_METING']].sum().sum()==0:
        continue
    df = df.rename({'NMPT_NORMOV_METING':'> norm','NMPT_ONDER_NORM_METING':'< norm','NMPT_NT_METING':'Niet toetsbaar'},axis='columns')
    index = [1,2,3,4,5,6,7,8,9,10,11,12]
    df = df.reindex(index)
    ax= df.plot(kind='bar', stacked=True, color=['red','limegreen','grey'], figsize = (9,6), xlim=(1,12))
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(reversed(handles), reversed(labels), loc='center left', bbox_to_anchor=(1, 0.5))
    ax.set_ylabel('Aantal meetlocaties')
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_xlabel('')
    sns.despine()
    plt.tight_layout()
    norm = norm.replace('/','_')
    plt.xticks(rotation=0)
    path = paths['output'] / f'Normoverschrijding stof/{stof}'
    path.mkdir(exist_ok=True, parents=True)
    plt.savefig(path / f'{stof} {teelt} {norm} normoverschrijding per maand')
    plt.close()

####################################################################################
### Voor alle teelten
for norm, stof in normovers[['NORM_OMSCHRIJVING','STOF_NAAM_SAM']].drop_duplicates().itertuples(index=False):
    teelt = 'Totaal'
    df = selectie(normovers, stof=stof, norm=norm, jaar=YEAR)
    if df['MLCMN_TEELT'].nunique()==1:
        continue
    if df['NMPT_NORMOV_METING'].sum()==0:
        continue
    df = df[['NMPT_NORMOV_METING','NMPT_ONDER_NORM_METING','NMPT_NT_METING','MAAND']].groupby('MAAND').sum()
    df = df.rename({'NMPT_NORMOV_METING':'> norm','NMPT_ONDER_NORM_METING':'< norm','NMPT_NT_METING':'Niet toetsbaar'},axis='columns')
    
    ax= df.plot(kind='bar', stacked=True, color=['red','limegreen','grey'], figsize = (9,6), xlim=(1,12))
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(reversed(handles), reversed(labels), loc='center left', bbox_to_anchor=(1, 0.5))
    ax.set_ylabel('Aantal meetlocaties')
    ax.set_xlabel('')
    sns.despine()
    plt.tight_layout()
    plt.xticks(rotation=0)
    norm = norm.replace('/','_')
    path = paths['output'] / f'Normoverschrijding stof/{stof}'
    path.mkdir(exist_ok=True, parents=True)
    plt.savefig(path / f'{stof} {teelt} {norm} normoverschrijding per maand')
    plt.close()
