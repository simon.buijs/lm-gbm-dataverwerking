# -*- coding: utf-8 -*-
"""
Make a map with the measuring locations from the LM-GBM with information of teelt
"""

import pandas as pd
import geopandas as gpd
from paths import set_paths
import matplotlib.pyplot as plt
from shapely.geometry import Point
import seaborn as sns
from constants import YEAR

sns.set('notebook')
sns.set_style('whitegrid')
paths = set_paths()

waterschappen = gpd.read_file(paths['waterschappen'])
metingen = pd.read_excel(paths["metingen"])
ows = gpd.read_file(paths['water'])
locaties = metingen.drop_duplicates(['MEETPUNT_CODE','JAAR'])
locaties['Teelt'] = locaties['MLCMN_TEELT'].str.capitalize()
locaties.to_csv(paths['output'] / 'Meetlocaties.csv')
geometry = [Point(xy) for xy in zip(locaties['XCOORD_M'], locaties['YCOORD_M'])]
gdf = gpd.GeoDataFrame(locaties, geometry=geometry, crs='epsg:28992')
gdf = gdf[gdf['JAAR']==YEAR]

for teelt in set(gdf['Teelt']):
    sel = gdf['Teelt'] ==teelt
    n = len(gdf[sel])
    gdf.loc[sel, 'Teelt'] = f'{teelt} ({n})'




fig, ax = plt.subplots(figsize=(6,8))
gdf.plot(column='Teelt', categorical=True, legend=True, ax=ax)
ows.plot(facecolor = 'lightblue', edgecolor='lightblue', linewidth=0.3, ax=ax, alpha=0.5 )
waterschappen.plot(facecolor='none', edgecolor='black', ax=ax, linewidth=0.3)
ax.axis('off')
plt.tight_layout()
plt.savefig(paths['output'] / 'meetlocaties.jpg', dpi=600)
