# -*- coding: utf-8 -*-
"""
Created on Mon Sep 23 09:14:40 2019

@author: buijs_sn
"""

import pandas as pd
import seaborn as sns; sns.set_context('talk')
import matplotlib.pyplot as plt
from functions import selectie
from paths import set_paths

## Filenames
paths= set_paths()
tabelj = pd.read_excel(paths['tabelJ'])
df = selectie(tabelj, norm='MAC-MKN')
labels = df.groupby('JAAR')['NMPT_GEMETEN'].sum()
df = df.groupby('JAAR')['NMPT_NORMOV_METING'].sum()
labels = (df / labels * 100).round(1).astype('str') + '%'

ax = df.plot(kind='bar', stacked=True, legend = None, figsize=(9,6))
plt.ylabel('Aantal MAC-overschrijdingen')
sns.despine()
rects = ax.patches
for rect, label in zip(rects, labels):
    height = rect.get_height()
    ax.text(rect.get_x() + rect.get_width() / 2, height , label,
            ha='center', va='bottom')
plt.xticks(rotation=45)
plt.xlabel('')
plt.tight_layout()
p = paths['output'] / 'Aantal MAC-overschrijdingen/'
p.mkdir(exist_ok=True, parents=True)
plt.savefig(p / 'Aantal MAC-overschrijdingen')
plt.close()

for teelt in list(set(tabelj['MLCMN_TEELT'])):
    df = selectie(tabelj, norm='MAC-MKN', teelt = teelt)
    labels = df.groupby('JAAR')['NMPT_GEMETEN'].sum()
    df = df.groupby('JAAR')['NMPT_NORMOV_METING'].sum()
    labels = (df / labels * 100)
    labels = [f'{x:.1f}%' for x in labels]
    ax = df.plot(kind='bar', stacked=True, legend = None, figsize=(9,6))
    plt.ylabel('Aantal MAC-overschrijdingen')
    sns.despine()
    rects = ax.patches
    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2, height , label,
                ha='center', va='bottom')
    plt.xticks(rotation=45)
    plt.xlabel('')
    plt.tight_layout()
    p = paths['output'] / 'Aantal individuele MAC-overschrijdingen/'
    p.mkdir(exist_ok=True, parents=True)
    plt.savefig(p / f'Aantal individuele MAC-overschrijdingen_{teelt}')
    plt.close()