# -*- coding: utf-8 -*-
"""
Created on Tue Aug 27 15:43:15 2019

@author: buijs_sn

Script om de data van het CML te verwerken. Van hun verschillende tabellen naar
output voor de rapportage.
"""
import pandas as pd
from functions import selectie
from paths import set_paths
from constants import WS_MAP, YEAR

## Filenames
paths = set_paths()

## Inlezen data
stoffenlijst = pd.read_excel(paths['stoffenlijst'])
stoffenlijst['Teelt'] = stoffenlijst['Teelt'].str.capitalize()
metingen = pd.read_excel(paths['metingen'])
metingen['WBHCODE_OMSCHRIJVING']= metingen['WBHCODE_OMSCHRIJVING'].map(WS_MAP)
metingen['MLCMN_TEELT'] = metingen['MLCMN_TEELT'].str.capitalize()
topstoffen = pd.read_excel(paths['tabelD'])
###########   Analyse tabel 2.3 aantal geanalyseerde stoffen per waterschap per teeltgroep ###############

    ## Aantal stoffen in stoffenlijst per teelt
nstoffen_per_teelt= stoffenlijst.groupby('Teelt')['Stof'].nunique()
nstoffen_per_teelt.index = nstoffen_per_teelt.index.str.capitalize()


## Aantal gemeten stoffen per teelt per waterschap
metingen2019 = selectie(metingen, jaar=YEAR)
metingen2018 = selectie(metingen, jaar=YEAR-1)

df = metingen2019.groupby(['MLCMN_TEELT','WBHCODE_OMSCHRIJVING'])['STOF_NAAM_SAM'].nunique().reset_index()
df.rename({'STOF_NAAM_SAM':f'Gemeten {YEAR}'}, axis=1, inplace=True)
df[f'Gemeten {YEAR-1}']= metingen2018.groupby(['MLCMN_TEELT','WBHCODE_OMSCHRIJVING'])['STOF_NAAM_SAM'].nunique().values
df['Stoffenlijst']= df['MLCMN_TEELT'].map(nstoffen_per_teelt)
df[f'% {YEAR-1}'] = df[f'Gemeten {YEAR-1}']/ df['Stoffenlijst']* 100
df[f'% {YEAR}'] = df[f'Gemeten {YEAR}']/ df['Stoffenlijst']* 100
df['Nmeetlocaties'] = metingen2019.groupby(['MLCMN_TEELT','WBHCODE_OMSCHRIJVING'])['code'].nunique().values
df = df[['MLCMN_TEELT','WBHCODE_OMSCHRIJVING','Stoffenlijst',f'Gemeten {YEAR}',f'% {YEAR}',f'Gemeten {YEAR-1}',f'% {YEAR-1}', 'Nmeetlocaties']]
df.to_excel(paths['output'] / 'tabel2,3.xlsx', index=False)
####################################################################################

############### Tabel 2.4 Overzicht normoverschrijdende stoffen die niet bij elk waterschap zijn geanalyseerd
df = selectie(topstoffen, jaar=YEAR)
df = df[(df.NMPT_GEMETEN != df.NMPT_MEETNET) & (df.NORMKLAS==11) & (df.INDEX > 0) & (df['MLCMN_TEELT']!='Alle_teelten')]
df = df[['MLCMN_TEELT','STOF_NAAM_SAM','NMPT_GEMETEN','NMPT_MEETNET']]
df.sort_values(by=['MLCMN_TEELT','STOF_NAAM_SAM'],inplace=True)

for teelt, stof in zip(df['MLCMN_TEELT'],df['STOF_NAAM_SAM']):
    nws = selectie(metingen2019, teelt=teelt.lower(), stof=stof)
    nws = nws['WBHCODE_OMSCHRIJVING'].nunique()
    totaalws = selectie(metingen2019,teelt=teelt.lower())['WBHCODE_OMSCHRIJVING'].nunique()
    df.loc[(df['MLCMN_TEELT']==teelt) & (df['STOF_NAAM_SAM']==stof),'#wsmetingen'] = nws
    df.loc[(df['MLCMN_TEELT']==teelt),'#ws'] = totaalws        

df.to_excel(paths['output'] / 'tabel2,4.xlsx', index=False)


