# -*- coding: utf-8 -*-
"""
Created on Tue Oct  8 12:10:00 2019

@author: buijs_sn
"""
import datetime

WS_MAP= {'Hoogheemraadschap De Stichtse Rijnlanden': 'De Stichtse Rijnlanden',
        'Hoogheemraadschap Hollands Noorderkwartier': 'Hollands Noorderkwartier',
        'Hoogheemraadschap van Delfland': 'Delfland',
        'Hoogheemraadschap van Rijnland': 'Rijnland',
        'Hoogheemraadschap van Schieland en Krimpene':'Schieland en Krimpenerwaard',
        'Waterschap AA en Maas':'Aa en Maas',
        'Waterschap Brabantse Delta': 'Brabantse Delta',
        'Waterschap De Dommel':'De Dommel',
        'Waterschap Drents Overijsselse Delta':'Drents Overijsselse Delta',
        'Waterschap Hollandse Delta':'Hollandse Delta',
        'Waterschap Hunze en Aa\'s':'Hunze en Aa\'s',
        'Waterschap Limburg':'Limburg',
        'Waterschap Noorderzijlvest':'Noorderzijlvest',
        'Waterschap Rijn en IJssel':'Rijn en IJssel',
        'Waterschap Rivierenland':'Rivierenland',
        'Waterschap Scheldestromen':'Scheldestromen',
        'Waterschap Vallei en Veluwe':'Vallei en Veluwe',
        'Waterschap Vechtstromen':'Vechtstromen',
        'Waterschap Zuiderzeeland':'Zuiderzeeland',
        'Wetterskip Fryslan':'Fryslân'}
YEAR = datetime.datetime.now().year - 1
LAST_YEAR = YEAR-1
START_YEAR = 2014
