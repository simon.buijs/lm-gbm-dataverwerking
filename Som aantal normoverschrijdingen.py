# -*- coding: utf-8 -*-
"""
Created on Thu Sep 19 08:14:44 2019

@author: buijs_sn
"""
from paths import set_paths
from functions import selectie
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
sns.set_context('talk')

# Filenames
paths = set_paths()

ds = pd.read_excel(paths['tabelB'])
ds.loc[ds['BOVEN_ONDER_NORM'] != 2, 'BOVEN_ONDER_NORM'] = 0
ds.loc[ds['BOVEN_ONDER_NORM'] == 2, 'BOVEN_ONDER_NORM'] = 1

df = ds.groupby(['STOF_NAAM_SAM', 'MLCMN_TEELT', 'JAAR', 'NORMKLAS']).agg(
    {'BOVEN_ONDER_NORM': 'sum', 'ID': 'count'}).reset_index()
df = df.rename({'NORMKLAS': 'NORM_OMSCHRIJVING'}, axis='columns')
df.loc[df['NORM_OMSCHRIJVING'] == 5, 'NORM_OMSCHRIJVING'] = 'MAC-MKN'
df.loc[df['NORM_OMSCHRIJVING'] == 11, 'NORM_OMSCHRIJVING'] = 'JG-MKN/MTR'
df = df.rename({'BOVEN_ONDER_NORM': 'Index', 'ID': 'Totaal'}, axis='columns')
alle_teelten = df.groupby(
    ['STOF_NAAM_SAM', 'JAAR', 'NORM_OMSCHRIJVING']).sum().reset_index()
alle_teelten['MLCMN_TEELT'] = 'Alle_teelten'
df = pd.concat([df, alle_teelten], sort=False)
topstoffen = df
topstoffen['MLCMN_TEELT'] = topstoffen['MLCMN_TEELT'].str.capitalize()


# Inlezen data
stoffenlijst = pd.read_excel(paths['stoffenlijst'])
stoffenlijst['Teelt'] = stoffenlijst['Teelt'].str.capitalize()
stof2014 = stoffenlijst[stoffenlijst['Datum_opname LM-GBM'].dt.year ==
                        2014].groupby('Teelt')['Stof'].unique()
stof2014['Alle_teelten'] = stoffenlijst[stoffenlijst['Datum_opname LM-GBM'].dt.year ==
                                        2014]['Stof'].drop_duplicates()
stof2017 = stoffenlijst[stoffenlijst['Datum_opname LM-GBM'].dt.year ==
                        2017].groupby('Teelt')['Stof'].unique()
stof2017['Alle_teelten'] = stoffenlijst[stoffenlijst['Datum_opname LM-GBM'].dt.year ==
                                        2017]['Stof'].drop_duplicates()
stof2018 = stoffenlijst[stoffenlijst['Datum_opname LM-GBM'].dt.year ==
                        2018].groupby('Teelt')['Stof'].unique()
stof2018['Alle_teelten'] = stoffenlijst[stoffenlijst['Datum_opname LM-GBM'].dt.year ==
                                        2018]['Stof'].drop_duplicates()
stof2019 = stoffenlijst[stoffenlijst['Datum_opname LM-GBM'].dt.year ==
                        2019].groupby('Teelt')['Stof'].unique()
stof2019['Alle_teelten'] = stoffenlijst[stoffenlijst['Datum_opname LM-GBM'].dt.year ==
                                        2019]['Stof'].drop_duplicates()
stof2020 = stoffenlijst[stoffenlijst['Datum_opname LM-GBM'].dt.year ==
                        2020].groupby('Teelt')['Stof'].unique()
stof2020['Alle_teelten'] = stoffenlijst[stoffenlijst['Datum_opname LM-GBM'].dt.year ==
                                        2020]['Stof'].drop_duplicates()


def make_plot(df, output, labels, legend=False, figsize=(9, 6)):
    ax = df.sum(axis=1).plot(kind='bar', legend=legend, figsize=figsize)
    plt.ylabel('Aantal normoverschrijdingen')
    sns.despine()
    plt.xticks(rotation=45, ha='right')
    rects = ax.patches
    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2, height, label,
                ha='center', va='bottom')
    plt.tight_layout()
    p = paths['output'] / 'Som Aantallen normoverschrijdingen'
    p.mkdir(exist_ok=True, parents=True)
    plt.savefig(p / output)
    plt.close()
    ax = df.plot(kind='bar', stacked=True, legend=legend, figsize=figsize)
    plt.ylabel('Aantal normoverschrijdingen')
    sns.despine()
    plt.xticks(rotation=45, ha='right')
    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2, height, label,
                ha='center', va='bottom')
    plt.tight_layout()
    p = paths['output'] / 'Som Aantallen normoverschrijdingen'
    output = output + '_totaal'
    p.mkdir(exist_ok=True, parents=True)
    plt.savefig(p / output)
    plt.close()


for norm in list(set(topstoffen['NORM_OMSCHRIJVING'])):
    normtekst = norm.replace('/', '_')
    for teelt in list(set(topstoffen['MLCMN_TEELT'])):
        da = selectie(topstoffen, teelt=teelt, norm=norm)
        da.columns = da.columns.str.capitalize()

        df = pd.DataFrame()
        for stoffenjaar, stoffen in zip(['Stoffen 2014', 'Stoffen 2017', 'Stoffen 2018', 'Stoffen 2019', 'Stoffen 2020'], [stof2014, stof2017, stof2018, stof2019, stof2020]):
            for jaar in sorted(list(set(topstoffen.JAAR))):
                try:
                    nstof = da[(da['Stof_naam_sam'].isin(
                        stoffen[teelt.capitalize()])) & (da['Jaar'] == jaar)]
                    df.loc[jaar, stoffenjaar] = nstof.groupby(
                        'Jaar')['Index'].sum().values[0]
                except:
                    df.loc[jaar, stoffenjaar] = 0
        labels = (df.sum(axis=1) / da.groupby('Jaar').sum()['Totaal'] * 100)
        labels = [f'{x:.1f}%' for x in labels]
        make_plot(df, output=f'{teelt}_{normtekst}', labels=labels)

        