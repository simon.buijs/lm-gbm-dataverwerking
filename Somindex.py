# -*- coding: utf-8 -*-
"""
Created on Tue Sep  3 16:47:56 2019
Maak figuren van de somindex over de tijd heen
@author: buijs_sn
"""
import pandas as pd
import seaborn as sns

sns.set_context("talk")
import matplotlib.pyplot as plt
from functions import selectie
from paths import set_paths

## Filenames
paths = set_paths()

## Inlezen data
somindex = pd.read_excel(paths["tabelM"])
somindex = selectie(somindex, norm="JG-MKN/MTR")
somindex = somindex[["JAAR", "MLCMN_TEELT", "SOMINDEX"]]

alt_index = pd.read_excel(paths["output"] / "alternatieve index.xlsx")

alt_index = (
    alt_index.groupby(["MLCMN_TEELT", "Jaar"]).sum().reset_index().drop("nmpt", axis=1)
)
alt_index.columns = ["Teelt", "Jaar", "Somindex"]
alt_index["Index"] = "Alternatieve index"
somindex.columns = ["Jaar", "Teelt", "Somindex"]
somindex["Index"] = "Somindex"
index = pd.concat([somindex, alt_index])

index = index[index["Teelt"] != "Alle_teelten"]
index = index.sort_values("Teelt")
g = sns.catplot(
    data=index,
    kind="bar",
    x="Jaar",
    y="Somindex",
    hue="Index",
    col="Teelt",
    ci=None,
    col_wrap=2,
)
plt.savefig(paths["output"] / "Somindex_teelten")


# Inlezen data
stoffenlijst = pd.read_excel(paths["stoffenlijst"])
stoffenlijst["Teelt"] = stoffenlijst["Teelt"].str.capitalize()
topstoffen = pd.read_excel(paths["tabelD"])


# Maak een dictionary met daarin per toelatingsjaar (in LMGBM) per teelt de stoffen
stof2017 = (
    stoffenlijst[stoffenlijst["Datum_opname LM-GBM"].dt.year == 2017]
    .groupby("Teelt")["Stof"]
    .unique()
)
stof2017["Alle_teelten"] = stoffenlijst[
    stoffenlijst["Datum_opname LM-GBM"].dt.year == 2017
]["Stof"].unique()
stof2018 = (
    stoffenlijst[stoffenlijst["Datum_opname LM-GBM"].dt.year == 2018]
    .groupby("Teelt")["Stof"]
    .unique()
)
stof2018["Alle_teelten"] = stoffenlijst[
    stoffenlijst["Datum_opname LM-GBM"].dt.year == 2018
]["Stof"].unique()
stof2019 = (
    stoffenlijst[stoffenlijst["Datum_opname LM-GBM"].dt.year == 2019]
    .groupby("Teelt")["Stof"]
    .unique()
)
stof2019["Alle_teelten"] = stoffenlijst[
    stoffenlijst["Datum_opname LM-GBM"].dt.year == 2019
]["Stof"].drop_duplicates()


def make_plot(df, output, stacked=False, legend=False, figsize=(9, 6)):
    ax = df.plot(kind="bar", stacked=stacked, legend=legend, figsize=figsize)
    plt.ylabel("Som-index [-]")

    for p in ax.patches:
        ax.annotate(
            format(p.get_height(), ".1f"),
            (p.get_x() + p.get_width() / 2.0, p.get_height()),
            ha="center",
            va="center",
            xytext=(0, 9),
            textcoords="offset points",
        )
    sns.despine()
    plt.xticks(rotation=45)
    plt.tight_layout()
    p = paths["output"] / "Somindex/"
    p.mkdir(exist_ok=True, parents=True)
    plt.savefig(p / output)
    plt.close()


for norm in list(set(topstoffen["NORM_OMSCHRIJVING"])):
    normtekst = norm.replace("/", "_")
    for teelt in list(set(somindex["MLCMN_TEELT"])):
        som = selectie(somindex, teelt=teelt, norm=norm)
        som.columns = som.columns.str.capitalize()
        som = som.set_index("Jaar")["Somindex"]

        make_plot(som, output=f"{teelt}_{normtekst}", stacked=True)

        da = selectie(topstoffen, teelt=teelt, norm=norm)
        da.columns = da.columns.str.capitalize()
        df = pd.DataFrame()
        for stoffenjaar, stoffen in zip(
            ["Stoffen 2017", "Stoffen 2018", "Stoffen 2019"],
            [stof2017, stof2018, stof2019],
        ):
            for jaar in sorted(list(set(topstoffen.JAAR))):
                try:
                    nstof = da[
                        (da["Stof_naam_sam"].isin(stoffen[teelt.capitalize()]))
                        & (da["Jaar"] == jaar)
                    ]
                    df.loc[jaar, stoffenjaar] = (
                        nstof.groupby("Jaar")["Index"].sum().values[0]
                    )
                except:
                    df.loc[jaar, stoffenjaar] = 0
        if df.sum().sum() > 0:
            df["Stoffen 2014"] = som - df.sum(axis=1)
            df = df.reindex(sorted(df.columns), axis=1)
            make_plot(
                df, output=f"{teelt}_{normtekst}_apart", stacked=True, legend=True
            )
