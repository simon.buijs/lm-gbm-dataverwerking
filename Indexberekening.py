# -*- coding: utf-8 -*-
"""
Created on Mon Sep 16 08:27:18 2019

@author: buijs_sn
"""
import seaborn as sns

sns.set_context("talk")
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import pandas as pd
import numpy as np
from functions import selectie
from paths import set_paths
from constants import YEAR

## Filenames
paths = set_paths()

metingen = pd.read_excel(paths["metingen"], parse_dates=["DATUM"])
normen = pd.read_excel(paths["normen"])[["STOF_NAAM_SAM", "MTR", "JG-MKN", "MAC-MKN"]]
normen = normen.set_index("STOF_NAAM_SAM") * 1000

alt_index = []
jaren = [2014, 2015, 2016, 2017, 2018, 2019, 2020]
for jaar in jaren:

    df = selectie(metingen, jaar=jaar)
    df = pd.merge(df, normen, on=["STOF_NAAM_SAM"], how="left")

    ## Stoffen die geen norm hebben
    geen_norm = df.drop_duplicates(["STOF_NAAM_SAM"])
    geen_norm = geen_norm[geen_norm["JG-MKN"].isna() & geen_norm["MTR"].isna()]
    geen_norm.to_csv(paths["output"] / "geen_norm.csv")

    ## Beneden detectiegrens / 2
    sel1 = df["BDETGRENS"] == 0
    df.loc[sel1, "MEETWAARDE"] = df.loc[sel1, "MEETWAARDE2"] / 2
    df.loc[~sel1, "MEETWAARDE"] = df.loc[~sel1, "MEETWAARDE2"]

    sel2 = ~df["MTR"].isna()
    sel3 = ~df["JG-MKN"].isna()
    sel4 = df["MTR"] < df["MEETWAARDE2"]
    sel5 = df["JG-MKN"] < df["MEETWAARDE2"]
    df["NT"] = 0
    df.loc[sel1 & sel2 & sel4, "NT"] = 1
    df.loc[sel1 & sel3 & sel5, "NT"] = 1
    df.loc[sel1, "Rgrens"] = df.loc[sel1, "MEETWAARDE2"]
    df.loc[df["NT"] == 1, "MEETWAARDE"] = np.nan

    def q90(x):
        return x.quantile(0.9)

    data = df.groupby(["STOF_NAAM_SAM", "MEETPUNT_CODE"]).agg(
        v_mean=("MEETWAARDE", "mean"),
        v_max=("MEETWAARDE", "max"),
        v_90=("MEETWAARDE", q90),
        RGmax=("Rgrens", "max"),
        nt=("NT", "mean"),
    )

    data = pd.merge(data.reset_index(), normen, on=["STOF_NAAM_SAM"], how="left")

    mtr = ~data["MTR"].isna()
    jg = ~data["JG-MKN"].isna()
    jg_onder = data["v_mean"] <= data["JG-MKN"]
    mtr_onder = data["v_90"] <= data["MTR"]
    jg_boven = data["v_mean"] > data["JG-MKN"]
    mtr_boven = data["v_90"] > data["MTR"]
    jg_5 = data["v_mean"] > (5 * data["JG-MKN"])
    mtr_5 = data["v_90"] > (5 * data["MTR"])
    niet_toetsbaar = data["nt"] == 1

    data.loc[mtr & mtr_onder, "Waarde"] = 0
    data.loc[jg & jg_onder, "Waarde"] = 0
    data.loc[mtr & mtr_boven, "Waarde"] = 1
    data.loc[jg & jg_boven, "Waarde"] = 1
    data.loc[mtr & mtr_5, "Waarde"] = 5
    data.loc[jg & jg_5, "Waarde"] = 5
    data.loc[niet_toetsbaar, "Waarde"] = 0
    data.loc[~niet_toetsbaar, "Toetsbaar"] = 1

    ## Koppel teelt aan meetlocatie
    teelt_loc = (
        metingen[["MEETPUNT_CODE", "MLCMN_TEELT"]]
        .drop_duplicates()
        .set_index("MEETPUNT_CODE")
        .squeeze()
    )
    data["MLCMN_TEELT"] = data["MEETPUNT_CODE"].map(teelt_loc)

    index = (
        data.groupby("STOF_NAAM_SAM")
        .agg(index=("Waarde", "mean"), nmpt=("MEETPUNT_CODE", "nunique"))
        .reset_index()
    )
    index["MLCMN_TEELT"] = "Alle_teelten"
    index_teelt = (
        data.groupby(["MLCMN_TEELT", "STOF_NAAM_SAM"])
        .agg(index=("Waarde", "mean"))
        .reset_index()
    )
    index = pd.concat([index, index_teelt], sort=False)
    index.to_excel(paths["output"] / f"index_{jaar}.xlsx", index=False)
    ### Maak de geaggregeerde waarde per jaar per stof per meetpunt
    ## N.B. Voor JG-MKN de gemiddelde / voor MTR 90percentiel / voor MAC de hoogste waarde

    ## Bepaal normoverschrijdende fractie NOF = 1-N/RGMax    per stof per meetpunt per jaar bij RG>N
    data["Norm"] = data["JG-MKN"].fillna(0) + data["MTR"].fillna(0)
    data.loc[niet_toetsbaar, "Waarde"] = (
        1 - data.loc[niet_toetsbaar, "Norm"] / data.loc[niet_toetsbaar, "RGmax"]
    )

    half = (
        data.groupby(["STOF_NAAM_SAM", "MLCMN_TEELT"])["Toetsbaar"].sum().reset_index()
    )
    half.loc[half["Toetsbaar"] < 5, "Aangepast"] = 0
    half.loc[half["Toetsbaar"] >= 5, "Aangepast"] = 1

    data = pd.merge(
        data,
        half.drop("Toetsbaar", axis=1),
        on=["STOF_NAAM_SAM", "MLCMN_TEELT"],
        how="left",
    )
    aangepast = (data["Aangepast"] == 1) & (data["nt"] == 1)

    ## Bereken het totaal aantal normoverschrijdingen / aantal toetsbare locaties
    toetsbaar = data[~niet_toetsbaar].copy()
    toetsbaar.loc[toetsbaar.Waarde > 0, "Waarde"] = 1
    toetsbaar = (
        toetsbaar.groupby(["STOF_NAAM_SAM", "MLCMN_TEELT"])
        .agg(tot=("Toetsbaar", "sum"), no=("Waarde", "sum"))
        .reset_index()
    )
    toetsbaar["NO/TOT"] = toetsbaar["no"] / toetsbaar["tot"]

    data = pd.merge(
        data,
        toetsbaar.drop(["no", "tot"], axis=1),
        on=["STOF_NAAM_SAM", "MLCMN_TEELT"],
        how="left",
    )
    data.loc[aangepast, "Waarde"] = (
        data.loc[aangepast, "Waarde"] + data.loc[aangepast, "NO/TOT"]
    ) / 2
    nieuwe_index = (
        data.groupby("STOF_NAAM_SAM")
        .agg(index=("Waarde", "mean"), nmpt=("MEETPUNT_CODE", "nunique"))
        .reset_index()
    )
    nieuwe_index["MLCMN_TEELT"] = "Alle_teelten"
    nieuwe_index_teelt = (
        data.groupby(["MLCMN_TEELT", "STOF_NAAM_SAM"])
        .agg(index=("Waarde", "mean"), nmpt=("MEETPUNT_CODE", "nunique"))
        .reset_index()
    )
    nieuwe_index = pd.concat([nieuwe_index, nieuwe_index_teelt], sort=False)

    nieuwe_index["Jaar"] = jaar
    alt_index.append(nieuwe_index)
nieuwe_index = pd.concat(alt_index)
nieuwe_index.to_excel(paths["output"] / f"alternatieve index.xlsx", index=False)
## Wanneer er in een stof ook wel toetsbare waarden zijn per stof per meetpunt per jaar.
# NOF = ((1-N/RG)+NO/TOTt)/2 als TOTt>=10 of 5 (voor index), en RG>N
# NO = aantal normoverschrijding. TOT = totaal aantal toetsbaar

file = data.groupby("STOF_NAAM_SAM").agg(
    norm_jg=("JG-MKN", "max"),
    norm_mtr=("MTR", "max"),
    rgmean=("RGmax", "mean"),
    rgmedian=("RGmax", "median"),
    rgmin=("RGmax", "min"),
    rgmax=("RGmax", "max"),
)

sel1 = file["norm_jg"].isna()
file.loc[sel1, "NOF_mean"] = file.loc[sel1, "rgmean"] / file.loc[sel1, "norm_mtr"]
file.loc[~sel1, "NOF_mean"] = file.loc[~sel1, "rgmean"] / file.loc[~sel1, "norm_jg"]
file.loc[sel1, "NOF_median"] = file.loc[sel1, "rgmedian"] / file.loc[sel1, "norm_mtr"]
file.loc[~sel1, "NOF_median"] = file.loc[~sel1, "rgmedian"] / file.loc[~sel1, "norm_jg"]
file.loc[sel1, "NOF_min"] = file.loc[sel1, "rgmin"] / file.loc[sel1, "norm_mtr"]
file.loc[~sel1, "NOF_min"] = file.loc[~sel1, "rgmin"] / file.loc[~sel1, "norm_jg"]
file.to_excel(paths["output"] / "verhouding rgmax tov norm.xlsx")
data.to_excel(paths["output"] / "datafile_index.xlsx")

## Aantal locaties niet-toetsbaar
normovers = pd.read_excel(paths["tabelH"])
norm = "JG-MKN/MTR"
normplus = selectie(normovers, jaar=YEAR, norm=norm)
normplus = normplus.groupby(["STOF_NAAM_SAM", "KLASSE_OMSCHRIJVING"])["NMPT"].sum()
normplus = normplus.unstack()
normplus["NMPT_GEMETEN"] = normplus.drop("geen metingen", axis=1).sum(axis=1)
nt = normplus[["niet toetsbaar", "NMPT_GEMETEN"]]
nt = nt.fillna(0)
##########  Maak plaatjes voor belangrijke n.t.-stoffen #######################################
nt.columns = ["NT", "Totaal"]
nt = nt.astype("int")

df = pd.merge(file, nt, on="STOF_NAAM_SAM")

## Maak tabel C4 #####################################
tabelC4 = nt["NT"] / nt["Totaal"] * 100
tabelC4 = tabelC4[tabelC4 > 0].astype("int")
tabelC4 = pd.DataFrame(tabelC4)
tabelC4 = tabelC4.rename({0: "Percentage niet toetsbaar"}, axis="columns")
tabelC4.sort_values(by="Percentage niet toetsbaar", inplace=True, ascending=False)
stoffenlijst = pd.read_excel(paths["stoffenlijst"])
tabelC4 = pd.merge(
    tabelC4,
    stoffenlijst[["Stof", "CAS-nummer", "Aquonaam"]].drop_duplicates(),
    left_on="STOF_NAAM_SAM",
    right_on="Stof",
    how="inner",
)
tabelC4 = tabelC4.set_index("Stof")
tabelC4 = tabelC4[tabelC4.columns[[1, 2, 0]]]
tabelC4["Percentage niet toetsbaar"] = (
    tabelC4["Percentage niet toetsbaar"].astype("str") + "%"
)
tabelC4.to_excel(paths["output"] / "tabelC4.xlsx")

nt = nt.sort_values(by="NT", ascending=True)

sns.set_context("talk")
ax = nt[-10:].plot(kind="barh", figsize=(8, 8))
ax.set_xlabel("Aantal meetlocaties")
ax.set_ylabel("")
sns.despine()
plt.tight_layout()
plt.savefig(paths["output"] / "percentage niet_toetsbaar")
plt.close()
##########################################################

nt = nt.join(file["NOF_median"], on="STOF_NAAM_SAM", how="left")
nt.to_csv(paths["output"] / "nt.csv", sep=";")
ax = nt[-10:]["NOF_median"].plot(kind="barh", figsize=(7, 8))
ax.set_xlabel("RG / norm")
ax.set_ylabel("")
plt.xscale("log")
ax.xaxis.set_major_formatter(
    ticker.FuncFormatter(
        lambda y, pos: ("{{:.{:1d}f}}".format(int(np.maximum(-np.log10(y), 0)))).format(
            y
        )
    )
)
sns.despine()
plt.tight_layout()
plt.savefig(paths["output"] / "niet_toetsbaar NOF")
plt.close()
