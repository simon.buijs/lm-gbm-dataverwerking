# -*- coding: utf-8 -*-
"""
Created on Tue Oct  8 11:58:15 2019

@author: buijs_sn
"""

from pathlib import Path
import datetime

YEAR = datetime.datetime.now().year - 1


def set_paths():
    input = Path("..\input")
    paths = {}
    paths["output"] = Path("..\output")
    paths["tabelA"] = (
        input / f"Tabel_A_Grafiek_Percentage_Normov_Meetpunten_{YEAR}_v1.xlsx"
    )
    paths["tabelB"] = (
        input / f"Tabel_B_Kaart_Mate_Normov_Meetpunten_2014_{YEAR}_v1.xlsx"
    )
    paths["tabelC"] = (
        input / f"Tabel_C_Kaart_Perc_Normov_Metingen_Meetpunten_2014_{YEAR}_v1.xlsx"
    )
    paths["tabelD"] = input / f"Tabel_D_Top_Stoffen_per_Teelt_2014_{YEAR}_v2.xlsx"
    paths["tabelE"] = (
        input / f"Tabel_E_Top_Teelten_normoverschrijdingen_2014_{YEAR}_v1.xlsx"
    )
    paths["tabelF"] = input / f"Tabel_F_Meetintensiteit_2014_{YEAR}_v1.xlsx"
    paths["tabelG"] = input / f"Tabel_G_Stoffen_Bronhouder_Teelt_2014_{YEAR}_v1.xlsx"
    paths["tabelH"] = (
        input / f"Tabel_H_Histogram_Mate_Normov_Meetpunten_klassen_2014_{YEAR}_v1.xlsx"
    )
    paths["tabelI"] = (
        input
        / f"Tabel_I_Hist_Perc_Normoverschrijdende_metingen_nmpt_teelt_stof_plus_NT_2014_{YEAR}_v1.xlsx"
    )
    paths["tabelJ"] = (
        input / f"Tabel_J_Hist_normov_stof_teelt_jaar_maand_2014_{YEAR}_v1.xlsx"
    )
    paths["tabelK"] = (
        input
        / f"Tabel_K_Meetintensiteit_Maximum_aantal_maanden_metingen_teelt_jaar_wbh_2014_{YEAR}_v1.xlsx"
    )
    paths["tabelL"] = (
        input
        / f"Tabel_L_Concentraties_in_de_tijd_per_stof_incl_lijn_rapgrenzen._2014_{YEAR}_v1.xlsx"
    )
    paths["tabelM"] = (
        input
        / f"Tabel_M_Som_Index_Normovstoffen_per_Teelt_per_jaar_2014_{YEAR}_v2.xlsx"
    )
    paths["stoffenlijst"] = input / "Stoffenlijst 2020.xlsx"
    paths["metingen"] = (
        input / "Metingen_BM_Meetnet_2014_2020_gecontroleerd_v1_isomeren.xlsx"
    )
    paths["metingen_jaar"] = (
        input / "Metingen_BM_Meetnet_2020_gecontroleerd_v1_isomeren.xlsx"
    )
    paths["normen"] = input / "overzicht_5 normen_bma_versie_2021_08_16.xlsx"
    paths["provincies"] = input / "provincies.shp"
    paths["water"] = input / "ows_nl.shp"
    paths["waterschappen"] = input / "waterschappen.shp"

    return paths
