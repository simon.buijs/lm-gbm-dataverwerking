# -*- coding: utf-8 -*-
"""
Created on Tue Sep 10 13:46:48 2019
Meetfrequentie LM-GBM Bijlage A
@author: buijs_sn
"""
import pandas as pd
from paths import set_paths
import seaborn as sns
import matplotlib.pyplot as plt
from functions import selectie
from constants import YEAR

sns.axes_style("white")
sns.set_style("ticks")
sns.set_context("notebook")

paths = set_paths()
## Filenames
df = pd.read_excel(paths["tabelF"])
df = selectie(df, jaar=YEAR)
locs = df.groupby(["MLCMN_TEELT"])["code"].nunique()
da = df.groupby(["MLCMN_TEELT", "MAAND"])["code"].nunique().reset_index()
da = da.pivot(index="MLCMN_TEELT", columns="MAAND", values="code").apply(
    lambda x: x / locs.values * 100, axis=0
)
da["aantal"] = locs
da.to_excel(paths["output"] / "meetfrequentie per maand.xlsx")

da = (
    df.groupby(["WBHCODE_OMSCHRIJVING", "code", "MLCMN_TEELT"])["MAAND"]
    .nunique()
    .reset_index()
)
da.to_excel(paths["output"] / "meetfrequentie per waterschap_teelt.xlsx")


df = pd.read_excel(paths["metingen"])
stof_teelt = df[["STOF_NAAM_SAM", "MLCMN_TEELT"]].drop_duplicates()
stof = df["STOF_NAAM_SAM"].drop_duplicates().reset_index()["STOF_NAAM_SAM"]

stoffenlijst = pd.read_excel(paths["stoffenlijst"])
sel = stoffenlijst["Datum_stoppen in LM-GBM"].isnull()
teelt_stof = stoffenlijst[sel][["Teelt", "Stof"]].drop_duplicates()

tekst = pd.DataFrame()
tekst["Stof_teelt combinaties_stof"] = teelt_stof["Stof"]
tekst["Stof_teelt combinaties_teelt"] = teelt_stof["Teelt"]
tekst["Unieke stoffen"] = stof

# Plot de stof_teelt combinatyies per jaar
stofteelt_jaar = df[["STOF_NAAM_SAM", "MLCMN_TEELT", "JAAR"]].drop_duplicates()
stofteelt_jaar = stofteelt_jaar.groupby(["JAAR"])["STOF_NAAM_SAM"].count()

ax = stofteelt_jaar.plot(kind="bar")
plt.xticks(rotation=45, ha="right")
sns.despine()
ax.bar_label(ax.containers[0])
plt.tight_layout()
plt.savefig(paths["output"] / "Stofteelt-combinaties per jaar")

stof_jaar = df[["STOF_NAAM_SAM", "JAAR"]].drop_duplicates()
stof_jaar = stof_jaar.groupby(["JAAR"])["STOF_NAAM_SAM"].count()

normoverschr = pd.read_excel(paths["tabelB"])
sel = normoverschr["BOVEN_ONDER_NORM"] == 2
normoverschr = normoverschr[sel]

stof_normover = normoverschr[["STOF_NAAM_SAM", "JAAR", "NORMKLAS"]].drop_duplicates()
stof_normover = stof_normover.groupby(["JAAR", "NORMKLAS"])["STOF_NAAM_SAM"].count()

normstoffen = pd.read_excel(paths["tabelB"])
normstoffen = normstoffen[["STOF_NAAM_SAM", "JAAR", "NORMKLAS"]].drop_duplicates()
normstoffen = normstoffen.groupby(["JAAR", "NORMKLAS"])["STOF_NAAM_SAM"].count()

stof_norm = pd.DataFrame()
stof_norm["Aantal"] = stof_normover
stof_norm["Percentage"] = (stof_normover / normstoffen * 100).round(0)
stof_norm.reset_index(inplace=True)
stof_norm = stof_norm.melt(id_vars=["JAAR", "NORMKLAS"])
stof_norm.columns = ["jaar", "norm", "Legenda", "normoverschrijdende stoffen"]
stof_norm["norm"] = stof_norm["norm"].map({5: "MAC-MKN", 11: "JG-MKN/MTR"})
stof_norm = stof_norm.sort_values("norm")
g = sns.catplot(
    data=stof_norm,
    kind="bar",
    x="jaar",
    y="normoverschrijdende stoffen",
    hue="Legenda",
    col="norm",
    ci=None,
)

sns.despine()
for ax in g.axes.flat:
    for container in ax.containers:
        ax.bar_label(container)
plt.savefig(
    paths["output"] / "Percentage normoverschrijdende stoffen per jaar",
    bbox_inches="tight",
)
    kind="bar",
    x="jaar",
    y="normoverschrijdende stoffen",
    hue="Legenda",
    col="norm",
    ci=None,
)

sns.despine()
for ax in g.axes.flat:
    for container in ax.containers:
        ax.bar_label(container)
plt.savefig(
    paths["output"] / "Percentage normoverschrijdende stoffen per jaar",
    bbox_inches="tight",
)
