# -*- coding: utf-8 -*-
"""
Created on Tue Sep  3 13:29:19 2019

Script om de data van het CML te verwerken. Van hun verschillende tabellen naar
output voor de rapportage.

Normoverschrijdingen bepalen H3
@author: buijs_sn
"""
import pandas as pd
import numpy as np
from functions import selectie
from paths import set_paths
from constants import WS_MAP, YEAR
## Filenames
paths= set_paths()
## Inlezen data
stoffenlijst = pd.read_excel(paths['stoffenlijst'])
metingen = pd.read_excel(paths['metingen'])
metingen['WBHCODE_OMSCHRIJVING']= metingen['WBHCODE_OMSCHRIJVING'].map(WS_MAP)
topstoffen = pd.read_excel(paths['tabelD'])
normovers = pd.read_excel(paths['tabelH'])

######################  Maken van excel-files met top normoverschrijdingen per norm/ teelt

for teelt in list(set(topstoffen['MLCMN_TEELT'])):
    for norm in list(set(topstoffen['NORM_OMSCHRIJVING'])):

        df = selectie(topstoffen, teelt=teelt, norm=norm)
        ranks = df.pivot(index = 'STOF_NAAM_SAM', columns = 'JAAR', values='INDEX')
        ranks = ranks.sort_values([YEAR,'STOF_NAAM_SAM'], ascending=False)
        df2019 = selectie(df, jaar=YEAR)[['RANK','STOF_NAAM_SAM']].set_index('STOF_NAAM_SAM')
        ranks = df2019.join(ranks, on='STOF_NAAM_SAM')
 
        if teelt == 'Alle_teelten':
            normplus = selectie(normovers, jaar=YEAR, norm=norm)
        else:
            normplus = selectie(normovers, jaar=YEAR, norm=norm, teelt=teelt)
            
        normplus = normplus.groupby(['STOF_NAAM_SAM','KLASSE_OMSCHRIJVING'])['NMPT'].sum()
        normplus = normplus.unstack()
        normplus['NMPT_GEMETEN'] = normplus.drop('geen metingen', axis=1).sum(axis=1)
        ranks = ranks.join(normplus, on='STOF_NAAM_SAM')
        
        ranks['percentage'] = (ranks['niet toetsbaar'] / ranks['NMPT_GEMETEN']*100).round(0)
        ranks.loc[ranks['niet toetsbaar'].notna(),'niet toetsbaar'] = ranks.loc[ranks['niet toetsbaar'].notna(),'niet toetsbaar'].astype('int').astype('str') + ' (' + ranks.loc[ranks['niet toetsbaar'].notna(),'percentage'].astype('int').astype('str') + '%)'
        
        if teelt == 'Alle_teelten':
            normplus = selectie(normovers, jaar=YEAR-1, norm=norm)
        else:
            normplus = selectie(normovers, jaar=YEAR-1, norm=norm, teelt=teelt)
            
        normplus = normplus.groupby(['STOF_NAAM_SAM','KLASSE_OMSCHRIJVING'])['NMPT'].sum()
        normplus = normplus.unstack()
        normplus['NMPT_GEMETEN'] = normplus.drop('geen metingen', axis=1).sum(axis=1)
        normplus.columns = [column + f' {YEAR-1}' for column in normplus.columns]
        ranks = ranks.join(normplus, on='STOF_NAAM_SAM')
        ranks[f'percentage_{YEAR-1}'] = (ranks[f'niet toetsbaar {YEAR-1}'] / ranks[f'NMPT_GEMETEN {YEAR-1}']*100)
        
        
        try:
            ranks[YEAR-1]
        except:
            ranks[YEAR-1] = np.nan
        try:
            ranks[YEAR-2]
        except:
            ranks[YEAR-2] = np.nan
        try:
            ranks['> 5*norm']
        except:
            ranks['> 5*norm'] = np.nan
        try:
            ranks['> norm']
        except:
            ranks['> norm'] = np.nan
        try:
            ranks[f'> 5*norm {YEAR-1}']
        except:
            ranks[f'> 5*norm {YEAR-1}'] = np.nan
        try:
            ranks[f'> norm {YEAR-1}']
        except:
            ranks[f'> norm {YEAR-1}'] = np.nan
            
        normtekst = norm.replace('/','_')
        jaren = list(range(YEAR-2, YEAR+1))
        path = paths['output'] / 'Ranking stoffen per teelt/Alle data'
        path.mkdir(exist_ok=True, parents=True)
        ranks.to_excel(path / f'{normtekst}_{teelt}.xlsx')
        ranks = ranks.reset_index().set_index('RANK')[['STOF_NAAM_SAM',*jaren ,'NMPT_GEMETEN','> norm','> 5*norm','niet toetsbaar']]
        ranks[jaren] = ranks[jaren].round(2)
        path = paths['output'] / 'Topstoffen'
        path.mkdir(exist_ok=True, parents=True)
        ranks.to_excel(paths['output'] / f'Topstoffen/{normtekst}_{teelt}.xlsx')
        ranks = ranks.fillna(0)
        sel1 = ranks[YEAR-2] == 0
        sel2 = ranks[YEAR-1] == 0
        sel3 = ranks[YEAR] != 0
        ranks = ranks[sel1 & sel2 & sel3]
        path = paths['output'] / 'Ranking stoffen per teelt/Nieuwe stoffen'
        path.mkdir(exist_ok=True, parents=True)
        ranks.drop([YEAR-2,YEAR-1], axis=1).to_excel(path / f'{normtekst}_{teelt}.xlsx')
        
        



