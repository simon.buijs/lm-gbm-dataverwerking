# -*- coding: utf-8 -*-
"""
Created on Tue Sep 10 09:36:25 2019
Functies
@author: buijs_sn
"""


def selectie(df, norm=None, jaar=None, teelt=None, stof=None):
    if norm != None:
        df = df[df["NORM_OMSCHRIJVING"] == norm]
    if jaar != None:
        df = df[df["JAAR"] == jaar]
    if teelt != None:
        df = df[df["MLCMN_TEELT"] == teelt]
    if stof != None:
        df = df[df["STOF_NAAM_SAM"] == stof]
    return df.copy()
